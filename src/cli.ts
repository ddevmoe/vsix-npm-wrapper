import * as path from 'path';
import * as args from 'args';
import { splitPathsByExistence, isFile, getVsixFiles } from './controllers/fs-controller';
import { transformVsixFile } from './controllers/vsix-controller';


const PROGRAM_HELP_NAME = 'vsix-wrap';

async function main() {
    args.option('paths', '.vsix files or directories', ['.']);
    args.option('dest', 'destination path', './wrapped-vsix');

    const flags = args.parse(process.argv, {
        name: PROGRAM_HELP_NAME
    });

    if (Object.keys(flags).length == 0) args.showHelp();
    
    console.debug('Resolving paths...');
    let paths = flags.paths.map(arg => path.resolve(arg));

    let { existing, missing } = splitPathsByExistence(paths);
    if (missing.length) console.warn(`The following paths are missing:\n${missing.join('\n')}`);

    if (!existing.length) {
        console.warn('None of the provided paths exist. Aborting...');
        process.exit();
    }

    let resolvedFiles = await Promise.all(existing.map(async p => await isFile(p) ? p : await getVsixFiles(p)));
    let vsixFiles: string[] = resolvedFiles.flat();

    if (!vsixFiles.length) {
        console.warn('No .vsix files were found in the given paths.');
        process.exit();
    }

    console.debug(`Detected .vsix files:\n"${vsixFiles.join('"\n"')}"`);
    
    let outDir = path.resolve(flags.dest);
    console.debug(`Dropping output folders at: "${outDir}"`);

    await Promise.all(vsixFiles.map(p => transformVsixFile(p, outDir)));

    console.info('Done!');
};

main();
