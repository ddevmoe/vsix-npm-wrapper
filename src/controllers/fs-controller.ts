import * as fs from 'fs';
import * as glob from 'glob';
import { VSIX_FILE_EXTENSION } from './vsix-controller';
import { promisify } from 'util';


export function splitPathsByExistence(paths: string[]): { existing: string[], missing: string[] } {
    let existing: string[] = [];
    let missing: string[] = [];
    
    paths.forEach(path => {
        if (fs.existsSync(path)) existing.push(path);
        else missing.push(path);
    });

    return { existing, missing };
}

export async function getVsixFiles(path: string): Promise<string[]> {
    return promisify(glob)(`${path}/*.${VSIX_FILE_EXTENSION}`);
}

export async function isFile(path: string): Promise<Boolean> {
    return fs.promises.lstat(path)
        .then(result => result.isFile());
}
