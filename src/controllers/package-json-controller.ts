import * as fs from 'fs';
import { VSIX_DEST_NAME } from './vsix-controller';


export const REQUIRED_PACKAGE_JSON_KEYS = [
    'name',
    'displayName',
    'description',
    'version',
    "author",
    'engines',
    'scripts',
    'publisher',
    'license'
];

export function filterObjectKeys(obj: any, keysWhitelist: string[]): {} {
    return Object.keys(obj)
        .filter((key) => keysWhitelist.includes(key))
        .reduce((res, key) => {
            res[key] = obj[key];
            return res;
        }, {});
}

export async function transformPackageJsonContents(filePath: string): Promise<void> {
    return fs.promises.readFile(filePath)
        .then(buffer => new Promise((resolve, reject) => {
            let content = JSON.parse(buffer.toString());
            resolve(filterObjectKeys(content, REQUIRED_PACKAGE_JSON_KEYS));
        }))
        .then(content =>  {
            content['files'] = [
                VSIX_DEST_NAME,  // Asterisk includes the extension, but the extension complains if we don't explicitly define a .vsix file
                '*'
            ];
            return content;
        })
        .then(content => fs.promises.writeFile(filePath, JSON.stringify(content, null, 2)))
        .catch(err => console.error(err));
}
