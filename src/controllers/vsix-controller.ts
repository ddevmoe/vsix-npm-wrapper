import * as unzipper from 'unzipper';
import * as path from 'path';
import * as fs from 'fs';


import { transformPackageJsonContents } from './package-json-controller';


export const VSIX_FILE_EXTENSION = 'vsix';
export const VSIX_DEST_NAME = `extension.${VSIX_FILE_EXTENSION}`
export const FILE_NAMES_TO_EXTRACT = [
    /package\.json/,
    /readme\.?.*/i,
    /changelog\.?.*/i,
    /icon\.(png|jpg|jpeg|svg)/i
];


async function extractFiles(zipFileLocation: string, fileMatches: RegExp[], outDirectory: string) {
    return unzipper.Open.file(zipFileLocation)
        .then(extractedFiles => {
            extractedFiles.files.forEach(file => {
                if (file.path.includes('node_modules')) return;
                let fileName = path.basename(file.path);

                if (fileMatches.some(re => re.test(fileName))) {
                    let outFilePath = path.join(outDirectory, fileName);
                    file.stream().pipe(fs.createWriteStream(outFilePath));
                }
            })
        });
}

export async function transformVsixFile(vsixPath: string, destDir: string): Promise<any> {
    let extName = path.basename(vsixPath);
    let extRootDir = path.join(destDir, extName.replace(`.${VSIX_FILE_EXTENSION}`, ''));

    return fs.promises.mkdir(extRootDir, { recursive: true })
        .then(() => extractFiles(vsixPath, FILE_NAMES_TO_EXTRACT, extRootDir))
        .then(() => fs.promises.copyFile(vsixPath, path.join(extRootDir, VSIX_DEST_NAME)))
        .then(() => transformPackageJsonContents(`${extRootDir}/package.json`));
}
